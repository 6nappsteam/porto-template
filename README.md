6nApps Canvas Template
======================

Intégration de Porto 7.2.0 template dans symfony 4.

Branch `master` of this bundle requires at least __PHP 7.1__ and __Symfony 4.2__ components

Installation
============

en console
+ composer require sixnapps/porto-template-bundle
+ bin/console assets:install --symlink

ajouter au fichier config/services.yaml du projet parent:

    ...
    services:
        ...
        sixnapps.porto.templating.services:
            class:  'Sixnapps\PortoTemplateBundle\Templating\Templating'
            public: true
            arguments:
                $templating: '@templating'
    
        sixnapps.porto.twig.services:
            class:  'Sixnapps\PortoTemplateBundle\Twig\TwigExtension'
            public: true
            arguments:
                $templating: '@sixnapps.porto.templating.services'
        ...
       
ajoute la route suivante au projet parent :

    sixnapps_porto:
        resource: "@SixnappsPortoTemplateBundle/Resources/config/routing.yaml"
        
+ lancer bin/console server:start
+ le template est accessible depuis : http://127.0.0.1:800x/porto-template

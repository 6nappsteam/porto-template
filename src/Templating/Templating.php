<?php

namespace Sixnapps\PortoTemplateBundle\Templating;

use Symfony\Component\Templating\EngineInterface;

class Templating
{
    private $templating;

    private $components;


    public function __construct( EngineInterface $templating )
    {
        $this->templating = $templating;
        $this->components = [
            'title'  => 'title/title',
            'header' => 'header/header',
            // *** FOOTERS ***

            'sticky-footer'            => 'footer/sticky-footer',
            'footer-classic'           => 'footers/classic',
            'footer-copyright'         => 'footers/copyright',
            'footer-liens'             => 'footers/liens',
            'footer'                   => 'footers/footer',

            // *** HEADERS ***
            'header-flat'              => 'headers/header-flat',
            'header-top'               => 'headers/header-top',
            'button-menu'              => 'buttons/button-menu',
            'language-menu'            => 'headers/language-menu',

            // *** CART ***
            'cart-header'              => 'cart/header',


            // *** SEARCH ***
            'search-bar'               => 'search/search-bar',

            // *** PAGE HEADERS ***
            'page-header-modern'       => 'page-headers/modern',
            'page-header-modern-coop'  => 'page-headers/modern-custom-coop',

            // *** BREADCRUMB ***
            'breadcrumb-1'             => 'breadcrumbs/breadcrumb-1',
            'breadcrumb-white-october' => 'breadcrumbs/breadcrumb-white-october',

            // *** CONTACT ***
            'newsletter-subscription'  => 'contact/newsletter-subscription',
            'info-contact'             => 'contact/info-contact',
            'socials-icons'            => 'contact/socials',
            // *** TWITTER ***
            'twitter-feed'             => 'contact/twitter-feed',

            // *** ELEMENTS ***

            'accordion'                   => 'accordion/item',
            'toggle'                      => 'toggle/item',
            'tab-default'                 => 'tab/default',
            'tab-simple'                  => 'tab/simple',
            'tab-navigation'              => 'tab/navigation',
            'tab-bottom'                  => 'tab/bottom',
            'rotator'                     => 'word-rotator/item',
            'animation'                   => 'animation/item',
            'counter'                     => 'counter/item',
            'table'                       => 'table/item',
            'modal'                       => 'modal/item',
            'sticky'                      => 'sticky/item',
            'service-box'                 => 'services/service-box',
            'service-banner'              => 'services/service-banner',
            'service-banner-animation'    => 'services/service-banner-animation',
            'service-carousel'            => 'services/service-carousel',
            'service-blockquote'          => 'services/service-blockquote',
            'lightBoxes-single'           => 'lightBoxes/single',
            'lightBoxes-gallery'          => 'lightBoxes/gallery',
            'lightBoxes-gallery-carousel' => 'lightBoxes/gallery-carousel',
            'lightBoxes-popup'            => 'lightBoxes/popup',
            'card'                        => 'card/item',
            'card-flip'                   => 'card/item-flip',
            'process'                     => 'process/item',
            'tooltip'                     => 'tooltip/item',
            'team-basic'                  => 'team/item-basic',
            'team-advanced'               => 'team/item-advanced',
            'before-after'                => 'before-after/item',
            'arrow'                       => 'arrow/item',
            'pricing-table'               => 'pricing-table/item',
            'call-to-action'              => 'call-to-actions/item',

            // *** PORTFOLIO ***
            'category-filter'             => 'category-filter/item',

            // *** FEATURES ***
            'slider-revo'                 => 'sliders/slider-revo',
            'slider-nivo'                 => 'sliders/slider-nivo',

            // *** STEPS ***
            'steps-home'                  => 'steps/steps-home',

            //SHOP
            'shop-four-columns'           => 'shop/fourColumns',
            'shop-focus-item'             => 'shop/focusItem',
            'shop-custom-focus-item'      => 'shop/customFocusItem',
            'shop-cart'                   => 'shop/cart',

            //GALLERY
            'image-gallery-default'       => 'galleries/image-gallery-default',

        ];
    }


    /**
     * @param $item
     * @param $data
     *
     * @return mixed
     */
    public function render( $item, $data )
    {
        if ( isset( $this->components[ $item ] ) && $this->templating->exists( '@SixnappsPortoTemplate/components/' . $this->components[ $item ] . '.html.twig' ) ) {
            return $this->templating->render( '@SixnappsPortoTemplate/components/' . $this->components[ $item ] . '.html.twig', $data );
        }
        return $this->templating->render(
            '@SixnappsPortoTemplate/components/not-found.html.twig', [
                'name' => $item,
            ]
        );
    }
}

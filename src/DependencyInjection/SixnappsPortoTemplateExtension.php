<?php

namespace Sixnapps\PortoTemplateBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

class SixnappsPortoTemplateExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     *
     * @throws \Exception
     */
    public function load( array $configs, ContainerBuilder $container )
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(dirname(__DIR__) . '/Resources/config'));

        $loaderParameter = new YamlFileLoader($container, new FileLocator(dirname(__DIR__) . '/Resources/config/parameter'));
        $loaderParameter->load('accordion.yaml');
        $loaderParameter->load('animation.yaml');
        $loaderParameter->load('arrow.yaml');
        $loaderParameter->load('call_to_action.yaml');
        $loaderParameter->load('card.yaml');
        $loaderParameter->load('category_filter.yaml');
        $loaderParameter->load('lightboxes.yaml');
        $loaderParameter->load('map.yaml');
        $loaderParameter->load('modal.yaml');
        $loaderParameter->load('pricing_table.yaml');
        $loaderParameter->load('process.yaml');
        $loaderParameter->load('services.yaml');
        $loaderParameter->load('table.yaml');
        $loaderParameter->load('tabs.yaml');
        $loaderParameter->load('team.yaml');
        $loaderParameter->load('toggle.yaml');
        $loaderParameter->load('tooltip.yaml');
        $loaderParameter->load('word_rotator.yaml');

        $container->setParameter('sixnapps_porto_template', $config);

    }
}


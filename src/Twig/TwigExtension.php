<?php
	
	namespace Sixnapps\PortoTemplateBundle\Twig;
	
	use Sixnapps\PortoTemplateBundle\Templating\Templating;
	use Symfony\Component\DependencyInjection\ContainerInterface;
	use Symfony\Component\HttpKernel\KernelInterface;
	use Twig_Extension;
	
	/**
	 * Class TwigExtension
	 *
	 * @package Sixnapps\PortoTemplateBundle\Twig
	 */
	class TwigExtension extends Twig_Extension
	{
		protected $kernel;
		private   $templating;
		private   $container;
		
		
		/**
		 * TwigExtension constructor.
		 *
		 * @param KernelInterface    $kernel
		 * @param Templating         $templating
		 * @param ContainerInterface $container
		 */
		public function __construct( KernelInterface $kernel, Templating $templating, ContainerInterface $container )
		{
			$this->kernel     = $kernel;
			$this->templating = $templating;
			$this->container  = $container;
		}
		
		
		/**
		 * @return array|\Twig_Function[]
		 */
		public function getFunctions()
		{
			return [
				new \Twig_SimpleFunction( 'bundleExists', [ $this, 'bundleExists' ], [ 'is_safe' => [ 'html' ] ] ),
				new \Twig_SimpleFunction( 'porto_component', [ $this, 'porto_component' ],
					[ 'is_safe' => [ 'html' ] ] ),
				new \Twig_SimpleFunction( 'porto_asset', [ $this, 'porto_asset' ], [ 'is_safe' => [ 'html' ] ] ),
				new \Twig_SimpleFunction( 'porto_assets', [ $this, 'porto_assets' ], [ 'is_safe' => [ 'html' ] ] ),
				new \Twig_SimpleFunction( 'default', [ $this, 'default' ], [ 'is_safe' => [ 'html' ] ] ),
			];
		}
		
		
		public function default( $name )
		{
			return $this->container->getParameter( $name );
		}
		
		
		/**
		 * @param       $item
		 * @param array $data
		 *
		 * @return mixed
		 */
		public function porto_component( $item, $data = [] )
		{
			return $this->templating->render( $item, $data );
		}
		
		
		/**
		 * @param $file
		 *
		 * @return string
		 */
		public function porto_asset( $file )
		{
			return 'https://cdn.6napps.com/template/porto-templates/porto-html.7.2.0/' . $file;
		}
		
		
		/**
		 * @param $jsfiles
		 *
		 * @return string
		 */
		public function porto_assets( $jsfiles )
		{
			$fileName = md5( implode( '-', $jsfiles ) );
			$root     = $this->kernel->getProjectDir();
			$file     = $root . '/public/assets/' . $fileName . '.js';
			if ( !file_exists( $file ) || $this->kernel->getEnvironment() == 'dev' ) {
				$content = '';
				foreach ( $jsfiles as $jsfile ) {
//					dump(dirname( dirname( ( __DIR__ ) ) ) . '/assets/' . $jsfile);
					$content .= file_get_contents( dirname( dirname( ( __DIR__ ) ) ) . '/assets/' . $jsfile ) . "\n";
				}
				file_put_contents( $file, $content );
			}
			return '<script src="/assets/' . $fileName . '.js"></script>';
		}
	}

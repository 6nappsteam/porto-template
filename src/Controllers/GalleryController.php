<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class GalleryController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class GalleryController extends AbstractController
	{

		/**
		 * @return Response
		 */
		public function image()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/image-gallery.html.twig' );
		}
	}

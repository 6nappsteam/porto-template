<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class PageServicesController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class PageServicesController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function services()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/services.html.twig' );
		}
	}

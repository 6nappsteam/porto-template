<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class MapController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class MapController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function map()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/map.html.twig' );
		}
	}

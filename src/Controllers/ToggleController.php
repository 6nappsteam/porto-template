<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class ToggleController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class ToggleController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function toggle()
		{

			return $this->render( '@SixnappsPortoTemplate/Pages/toggle.html.twig' );
		}
	}

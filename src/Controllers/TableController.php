<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class TableController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class TableController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function table()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/table.html.twig' );
		}
	}

<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class HeadersController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class HeadersController extends AbstractController
	{

		/**
		 * @return Response
		 */
		public function flat()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/headers-flat.html.twig' );
		}
	}

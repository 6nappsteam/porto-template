<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class TabController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class TabController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function tab()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/tab.html.twig' );
		}
	}

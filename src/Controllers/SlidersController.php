<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class SlidersController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class SlidersController extends AbstractController
	{

		/**
		 * @return Response
		 */
		public function show()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/sliders.html.twig' );
		}
	}

<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class PricingTableController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class PricingTableController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function pricingTable()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/pricing-table.html.twig' );
		}
	}

<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;


	/**
	 * Class CallToActionController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class CallToActionController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function show()
		{
			return $this->render('@SixnappsPortoTemplate/Pages/call-to-action.html.twig');
		}
	}

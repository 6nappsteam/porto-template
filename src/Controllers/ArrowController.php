<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class ArrowController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class ArrowController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function arrow()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/arrow.html.twig' );
		}
	}

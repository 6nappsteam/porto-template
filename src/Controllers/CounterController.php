<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class CounterController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class CounterController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function counter()
		{
			$counters = [
				'config' => [
					'class' => 'alternative-font',
					'color' => 'tertiary',
					'size'  => 'lg',
				],
				'items' => [
					[
						'label' => 'By month...',
						'end' => '1000',
						'append' => '+',
						'color' => 'secondary',
						'class' => 'alternative-font',
						'speed' => '1000',
						'refreshInterval' => '100',
						'decimals' => '2',
						'prefix' => '~',
						'suffix' => '€',
						'animation' => 'bounceIn',
						'animationDelay' => '100',
						'icon' => 'fas fa-coffee',
						'borders' => false,
						'iconSide' => false,
						'inline' => false

					],
				]
			];

			return $this->render('@SixnappsPortoTemplate/Pages/counter.html.twig', [
				'counters' => $counters,
			]);
		}
	}

<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class WordRotatorController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class WordRotatorController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function wordRotator()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/word-rotator.html.twig' );
		}
	}

<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class IndexController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class IndexController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function index()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/index.html.twig' );
		}
	}

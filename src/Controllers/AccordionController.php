<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class AccordionController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class AccordionController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function accordion()
		{
//			$accordions = [
//				[
//					'title' => 'Test de mon travail',
//					'items' => [
//						[
//							'title' => 'Item n°1',
//							'content' => '<strong>Qui</strong> que quoi donc <strong>où</strong> ? Mais <strong>pourquoi</strong> cela ?',
//							'icon' => [
//								'name' => 'file',
//								'color' => 'quaternary'
//							],
//						],
//						[
//							'title' => 'Item n°2',
//							'content' => 'Mais <i>où est</i> donc <i>or <strong>ni</strong> car</i> ?',
//							'icon' => [
//								'name' => 'star',
//								'color' => 'primary'
//							],
//						],
//					],
//					'parameters' => [
//						'card' => [
//							'accordion-quaternary'
//						],
//						'head' => [
//							'text-color-white'
//						],
//						'size' => 'col'
//					]
//				]
//			];

			return $this->render( '@SixnappsPortoTemplate/Pages/accordion.html.twig' );
		}
	}

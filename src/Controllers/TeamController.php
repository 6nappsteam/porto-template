<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class TeamController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class TeamController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function basic()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/team.html.twig', [
				'page' => 'basic',
			] );
		}


		/**
		 * @return Response
		 */
		public function advanced()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/team.html.twig', [
				'page' => 'advanced',
			] );
		}

	}

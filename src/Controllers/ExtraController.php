<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class ExtraController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class ExtraController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function error404()
		{
			$item = [
				'title'   => '404 !',
				'message' => 'Nous sommes désolés, il semblerait que vous essayez de consulter une page non existante.',
			];

			return $this->render( '@SixnappsPortoTemplate/Pages/error-404.html.twig', [
				'item' => $item,
			] );
		}


		/**
		 * @return Response
		 */
		public function error500()
		{
			$item = [
				'title'   => '500 !',
				'message' => 'Une erreur inattendue s\'est produite.',
			];

			return $this->render( '@SixnappsPortoTemplate/Pages/error-500.html.twig', [
				'item' => $item,
			] );
		}


		/**
		 * @return Response
		 */
		public function maintenance()
		{
			$item = [
				'title'   => 'Maintenance en cours',
				'message' => '<p>Le site est actuellement en maintenance <br> S\'il vous plaît veuillez revenir plus tard </p>.',
			];

			$boxes = [
				[
					'title'     => 'De quoi s\'agit-il',
					'content'   => 'Une maintenance permet de d\'améliorer le site et donc votre expérience. Il peut s\'agir d\'un problème à résoudre comme de nouvelles fonctionnalités',
					'icon'      => 'life-ring',
					'animation' => 'fadeInLeftShorter',
					'delay'     => '600',
					'size'      => 'col-lg-4',
				],
				[
					'title'   => 'De retour plus tard',
					'content' => 'Nous ferons toujours notre possible pour que la maintenance dure le moins de temps possible. Restez donc à l\'affût nous reviendrons très vite !',
					'icon'    => 'clock',
				],
				[
					'title'     => 'Prenez contact avec nous',
					'content'   => 'Si malheureusement la maintenance tombe à un moment crucial, n\'aillez crainte. Nous restons à votre disposition à tout moment !',
					'icon'      => 'envelope',
					'animation' => 'fadeInRightShorter',
				],
			];

			return $this->render( '@SixnappsPortoTemplate/Pages/maintenance.html.twig', [
				'item'  => $item,
				'boxes' => $boxes,
			] );
		}
	}

<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class LightBoxesController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class LightBoxesController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function lightBoxes()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/lightBoxes.html.twig' );
		}
	}

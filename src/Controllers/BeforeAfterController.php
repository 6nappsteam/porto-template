<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class BeforeAfterController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class BeforeAfterController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function beforeAfter()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/before-after.html.twig' );
		}
	}

<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class TooltipController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class TooltipController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function tooltip()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/tooltip.html.twig' );
		}
	}

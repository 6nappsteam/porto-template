<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class PageHeadersController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class PageHeadersController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function modern()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/page-headers-modern.html.twig' );
		}
	}

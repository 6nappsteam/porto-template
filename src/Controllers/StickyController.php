<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class StickyController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class StickyController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function sticky()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/sticky.html.twig' );
		}
	}

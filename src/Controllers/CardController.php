<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class CardController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class CardController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function card()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/card.html.twig' );
		}
	}

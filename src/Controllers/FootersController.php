<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class FootersController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class FootersController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function classic()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/footers-classic.html.twig' );
		}


		/**
		 * @return Response
		 */
		public function modern()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/footers-modern.html.twig' );
		}
	}

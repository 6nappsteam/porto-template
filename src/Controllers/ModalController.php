<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class ModalController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class ModalController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function modal()
		{
			$modals = [
				[
					'buttonMessage' => 'Cliques moi dessus',
					'title'         => 'Titre de la modale',
					'content'       => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="#">vehicula</a> lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus.</p>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="#">vehicula</a> lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus.</p>',
					'size'          => 'lg',
					'animation'     => TRUE,
				],
			];

			return $this->render( '@SixnappsPortoTemplate/Pages/modal.html.twig', [
				'modals' => $modals,
			] );
		}
	}

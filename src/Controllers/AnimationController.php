<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class AnimationController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class AnimationController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function animation()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/animation.html.twig' );
		}
	}

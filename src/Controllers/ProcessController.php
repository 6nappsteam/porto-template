<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class ProcessController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class ProcessController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function process()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/process.html.twig' );
		}
	}

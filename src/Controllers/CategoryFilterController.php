<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class CategoryFilterController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class CategoryFilterController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function filter()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/category-filter.html.twig' );
		}
	}

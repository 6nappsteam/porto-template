<?php

	namespace Sixnapps\PortoTemplateBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class ShopController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class ShopController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function shopFourColumns()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/shopFourColumns.html.twig' );
		}


		/**
		 * @return Response
		 */
		public function shopFocusItem()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/shopFocusItem.html.twig' );
		}


		/**
		 * @return Response
		 */
		public function shopCart()
		{
			return $this->render( '@SixnappsPortoTemplate/Pages/shopCart.html.twig' );
		}
	}

